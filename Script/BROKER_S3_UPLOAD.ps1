Write-Host ================================================================================
#$Path = "Z:\AWS\"
#$ArchivePath = ($Path,"Archive\")

$Path = "\\10.64.85.45\bi\AWS\"
$ArchivePath = (-join($Path,"Archive\"))
$profile = "quannl-068106932164"

$date = (Get-Date -Format "MM-dd-yyyy HH:mm K")
$NameSuffix = (Get-Date -Format "MMddyyyy_HHmm")

$bucket = "prod-acbs-com-vn"
$KeyPrefix = "acbs-ekyc/"

Write-Host ("Upload start: ",$date)
#Upload and copy Broker.txt
Write-Host "Upload and move Broker.txt"
$destination = (-join($ArchivePath,"Broker_",$NameSuffix,".txt"))
$source = (-join($Path,"Broker",".txt"))
$Key = (-join($KeyPrefix,"Broker",".txt"))
Write-S3Object -ProfileName $profile -BucketName "$bucket" -Key "$Key" -File "$source"
Copy-Item -Path "$source" -Destination "$destination"

#Upload and copy ClientPS.txt
Write-Host "Upload and move ClientPS.txt"
$destination = (-join($ArchivePath,"ClientPS_",$NameSuffix,".txt"))
$source = (-join($Path,"ClientPS",".txt"))
$Key = (-join($KeyPrefix,"ClientPS",".txt"))
Write-S3Object -ProfileName $profile -BucketName "$bucket" -Key "$Key" -File "$source"
Copy-Item -Path "$source" -Destination "$destination"

#Upload and copy ClientCS.txt
Write-Host "Upload and move ClientCS.txt"
$destination = (-join($ArchivePath,"ClientCS_",$NameSuffix,".txt"))
$source = (-join($Path,"ClientCS",".txt"))
$Key = (-join($KeyPrefix,"ClientCS",".txt"))
Write-S3Object -ProfileName $profile -BucketName "$bucket" -Key "$Key" -File "$source"
Copy-Item -Path "$source" -Destination "$destination"

$date = (Get-Date -Format "MM-dd-yyyy HH:mm K")
Write-Host ("End: ",$date)
Write-Host ================================================================================
Exit
