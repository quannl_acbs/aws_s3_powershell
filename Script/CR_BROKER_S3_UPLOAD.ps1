$LogName = "BROKER_S3_UPLOAD_LOG.txt"
$ScriptName = "BROKER_S3_UPLOAD.ps1"

$ScriptFile = (-join("\\10.64.85.45\bi\AWS\Script\",$ScriptName))
$LogFile = (-join("\\10.64.85.45\bi\AWS\Script\",$LogName))

Start-Transcript -Path "$LogFile" -Append

& "$ScriptFile"

Stop-Transcript
Exit